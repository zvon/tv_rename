#ifndef PROGRESS_HPP
#define PROGRESS_HPP

#include <string>

#ifdef _WIN32
#include <Windows.h>
using string = std::wstring;
#else
using string = std::string;
#endif

#define PROGRESS_PERC 0x4000
#define PROGRESS_STRING 0x4001

class ProgressBar {
public:
#ifndef GUI
    ProgressBar() = default;
    ~ProgressBar() = default;
#else
#ifndef _WIN32
    explicit ProgressBar( void *pw ) : ptr( pw ){};
#else
    explicit ProgressBar( void *hwnd ) : ptr( hwnd ){};
#endif // _WIN32
    ~ProgressBar();
#endif // GUI
    void print( int perc );
    void print( const string &t );
#ifdef GUI
private:
    void *ptr;
#endif
};

#endif
