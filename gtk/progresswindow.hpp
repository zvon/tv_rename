#ifndef GTKMM_PROGRESS_WINDOW
#define GTKMM_PROGRESS_WINDOW

#include <gtkmm/label.h>
#include <gtkmm/layout.h>
#include <gtkmm/progressbar.h>
#include <gtkmm/window.h>

class ProgressWindow : public Gtk::Window {
public:
    ProgressWindow();
    virtual ~ProgressWindow() = default;

    void setPerc( int perc ) {
        pb->set_fraction( perc / 100.0 );
    }
    void setLabel( const std::string &text ) {
        label->set_text( text );
    }

private:
    std::unique_ptr< Gtk::Layout > layout{ new Gtk::Layout() };
    std::unique_ptr< Gtk::Label > label{ new Gtk::Label() };
    std::unique_ptr< Gtk::ProgressBar > pb{ new Gtk::ProgressBar() };
};

#endif // GTKMM_MAIN_WINDOW
