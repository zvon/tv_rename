#ifndef TV_FUNCTIONS_H
#define TV_FUNCTIONS_H

#include <map>
#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "network.hpp"

#ifdef _WIN32

using string = std::wstring;
using char_t = wchar_t;

std::wstring utf8_to_wstring( const std::string &utf8 );
std::wstring LMsg( int id, ... );

#define _( x, ... ) LMsg( x, ##__VA_ARGS__ ).c_str()

#else

#include <libintl.h>

using string = std::string;
using char_t = char;

std::string getlocalized( const char *id, ... );

#define TEXT( x ) x
#define _( x, ... ) getlocalized( x, ##__VA_ARGS__ )

#endif

#ifdef __APPLE__
void error( int status, int i_errno, const char *fmt, ... );
#endif

#ifndef GUI
// CLI functions
void printHelp();
void printPatternHelp();

bool searchSeason( const char_t *const path, size_t &season_pos );
bool searchSeason( const char_t *const path, size_t &season_pos,
                   size_t &ep_pos );

void parseSeasonNumbers( std::set< int > &seasons_num, const char_t *argument );

#endif // GUI

string encodeUrl( const string &url );
string userHome();

void prepareDB( const string &_pattern = TEXT( "" ) );
#ifndef GUI
void addToDB( string &show, const string &path, const string &language,
              bool unix_names, bool dvd );
#else
void addToDB( const string &show, const string &path, const string &language,
              const string &id, const string &pattern, bool unix_names,
              bool dvd, void *progress_ptr );
#endif
void removeFromDB( const string &path );
void changeDBPattern( const string &pattern );
#ifndef GUI
void refreshDB( bool unix_names );
void updateDB( bool unix_names );
void cleanDB();
#else
void refreshDB( bool unix_names, void *progress_ptr );
void updateDB( bool unix_names, void *progress_ptr );
void cleanDB( void *progress_ptr );
#endif

#ifdef GUI
std::vector< std::unordered_map< string, string > > dbGetShows();
void changeDB( size_t index, const string &path, const string &language,
               const string &id, bool dvd );
void refreshSelectDB( std::unordered_set< size_t > indexes, bool unix_names,
                      void *progress_ptr );
void renameFiles( const std::vector< std::tuple< int, string, string, string > >
                      &renamed_files );
#endif

void iterateFS( std::map< int, std::map< int, string > > &seasons,
                const string &path );

string compilePattern( const string &pattern, int season, int episode,
                       const string &filename, const string &episodeName,
                       const string &showName );
string getDBPattern();

#endif
