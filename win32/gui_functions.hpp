#include <commctrl.h>
#include <string>
#include <windows.h>

void registerWindowClasses( HINSTANCE hInstance );
void centerWindow( HWND hwnd );

HWND createLabel( const wchar_t *text, int x, int y, int width, int height,
                  long long id, HWND parent, HFONT hFont );
HWND createEditBox( int x, int y, int width, int height, long long id,
                    HWND parent, HFONT hFont );
HWND createCombo( int x, int y, int width, int height, long long id,
                  HWND parent, HFONT hFont );
HWND createButton( const wchar_t *text, int x, int y, int width, int height,
                   long long id, HWND parent, HFONT hFont );
HWND createCheckbox( const wchar_t *text, int x, int y, int width, int height,
                     long long id, HWND parent, HFONT hFont );
HWND createProgressbar( int x, int y, int width, int height, long long id,
                        HWND parent, HFONT hFont );
HWND createListView( int x, int y, int width, int height, long long id,
                     HWND parent, HFONT hFont );
LVCOLUMNW addColumnToListView( HWND list_view, const wchar_t *text, int width,
                               int pos );

void addItemToCombo( HWND combo, const wchar_t *text );
void addItemToListView( HWND list_view, int row, int column,
                        const wchar_t *text );
void setListViewItemText( HWND list_view, int row, int column,
                          const wchar_t *text );
std::wstring getItemText( HWND hwnd, int row, int column );
int getComboCurSel( HWND hwnd );
std::wstring getDir();
void setIcons( HINSTANCE hInstance, HWND window );
