#include <string>
#include <windows.h>

class PatternWindow {
public:
    PatternWindow( HINSTANCE hInstance, const wchar_t *pattern,
                   HWND parent_window );

    void mainLoop();
    static LRESULT CALLBACK messageHandler( HWND hwnd, UINT umsg, WPARAM wParam,
                                            LPARAM lParam );
    bool accepted();
    const wchar_t *getPattern();

    ~PatternWindow() {
        EnableWindow( parent, true );
        SetFocus( parent );
    }

private:
    enum DialogResult { DIA_OK, DIA_Cancel };

    void storePattern();

    HWND window;
    HWND pattern_input;
    HWND parent;
    enum DialogResult result;
    wchar_t pattern[2048];
    const int window_width{ 215 };
    const int window_height{ 150 };

    static PatternWindow *pw;
};
