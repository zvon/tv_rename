#include <string>
#include <windows.h>

class ProgressWindow {
public:
    ProgressWindow( HINSTANCE hInstance, HWND parent_window );

    void mainLoop();
    static LRESULT CALLBACK messageHandler( HWND hwnd, UINT umsg, WPARAM wParam,
                                            LPARAM lParam );
    HWND getWindow();

    ~ProgressWindow() {
        EnableWindow( parent, true );
        SetFocus( parent );
    }

private:
    HWND window;
    HWND progress_bar;
    HWND progress_label;
    HWND parent;

    const int window_width{ 350 };
    const int window_height{ 100 };
    static ProgressWindow *pw;
};
