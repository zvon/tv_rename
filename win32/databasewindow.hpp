#include <string>
#include <tuple>
#include <vector>
#include <windows.h>

class DatabaseWindow {
public:
    DatabaseWindow(
        HINSTANCE hInstance,
        std::vector< std::pair< std::wstring, std::wstring > > &languages,
        HWND parent_window );

    void mainLoop();
    static LRESULT CALLBACK messageHandler( HWND hwnd, UINT umsg, WPARAM wParam,
                                            LPARAM lParam );

    ~DatabaseWindow() {
        EnableWindow( parent, true );
        SetFocus( parent );
    }

private:
    void listNotify( LPARAM lParam );
    void changed( int indes );
    void save();

    HWND window;
    HWND lang_combo;
    HWND list_hwnd{ nullptr };
    HWND cancel_button{ nullptr };
    HWND ok_button{ nullptr };
    HWND save_button{ nullptr };
    HWND parent;
    HINSTANCE hInst;
    int cur_row;
    const int window_width{ 550 };
    const int window_height{ 370 };
    std::vector< int > changed_rows;
    const std::vector< std::pair< std::wstring, std::wstring > > &langs;
    std::vector< std::tuple< int, int, std::wstring > > show_ids;
    static DatabaseWindow *dw;
};
