[![pipeline status](https://gitlab.com/zvon/tv_rename/badges/master/pipeline.svg)](https://gitlab.com/zvon/tv_rename/-/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/9aace967ca614da4bdb7a500f63858dd)](https://www.codacy.com/manual/tom.zvon/tv_rename?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=zvon/tv_rename&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/9aace967ca614da4bdb7a500f63858dd)](https://www.codacy.com/manual/tom.zvon/tv_rename?utm_source=gitlab.com&utm_medium=referral&utm_content=zvon/tv_rename&utm_campaign=Badge_Coverage)
# Rename TV Show files
This program downloads information about TV show episodes from <http://thetvdb.com> and renames episode files according to supplied name pattern.

For this to work your file needs to contain the season and episode number (in accordance to thetvdb) in this format: S#E# (doesn't matter if lower or upper case)

## Usage
You can either just rename files in a directory (this is done recursively, so you can enter a directory with multiple seasons, but all the files have to be from a single show). Or you can add a directory to local database, so when you add a new episode to the directory you just need to run database update for the file to be renamed.

Arguments:

`-s, --show` - Name of the show

`-n, --season` - Seasons to be renamed (numbers separated by space or 'all' for all seasons in the directory and sub directories)

`-d, --dvd` - use DVD ordering instead of aired ordering

`-c, --correct-path` - Don't ask if path is correct

`-t, --trust` - Don't prompt before changing file names

`-x, --linux` - Don't replace characters that are illegal in NTFS

`-l, --lang` - Language of the episode titles

`--print_langs` - Print available languages

`--name-pattern` - Pattern by which the files should be renamed

-   %filename - original filename
-   %show - show name from thetvdb
-   %epname - episode name from thetvdb
-   %season - season number, possible to specify leading 0 like this: %2season (number means how many leading zeros)
-   %episode - episode number, possible to specify leading 0 like this: %3season (number means how many leading zeros)

`--db-add` - Add path to database

`--db-refresh` - Refresh episode names for all paths in the database

`--db-update` - Check all paths in the database, if they contain new files, rename them

`--db-name-pattern` - Change name pattern used for files managed by the database

`--db-clean` - Remove deleted files from the database

`--db-remove` - Remove path from the database

## Installation

For installation of command line version you'll need:

-   *libcurl-dev* (or your distro's equivalent)
-   *sqlite-dev* (or your distro's equivalent)
-   *rapidjson*
-   some sort of C++ compiler (*clang++* for example)
-   *make*

When you have all prerequisites installed you need to do this sequence of commands:

    git clone 'https://gitlab.com/zvon/tv_rename.git'
    cd tv_rename_cpp
    git submodule update --init
    make
    sudo make install

For installation of GUI version you'll need:

-   *libcurl-dev* (or your distro's equivalent)
-   *sqlite-dev* (or your distro's equivalent)
-   *libgtkmm-dev* (or your distro's equivalent)
-   *rapidjson*
-   some sort of C++ compiler (*clang++* for example)
-   *make*

When you have all prerequisites installed you need to do this sequence of commands:

    git clone 'https://gitlab.com/zvon/tv_rename_cpp'
    cd tv_rename_cpp
    git submodule update --init
    make gui
    sudo make install_gui

You can uninstall both versions by running either `sudo make uninstall` or `sudo make uninstall_gui`

## 3rd party libraries

This project contains code from the following 3rd party libraries:
-   sqlite3 - <https://sqlite.org/>
