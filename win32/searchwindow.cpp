#pragma comment( linker, "\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"" )
#include "searchwindow.hpp"
#include "../functions.hpp"
#include "../resources_windows.h"
#include "../tv_rename.hpp"
#include "gui_functions.hpp"

#define ID_SHOW_STATIC 0x0001
#define ID_LANG_STATIC 0x0002
#define ID_RES_STATIC 0x0002
#define ID_SHOW_INPUT 0x1000
#define ID_LANG_INPUT 0x1001
#define ID_RES_INPUT 0x1002
#define ID_SEARCH_BUTTON 0x1003
#define ID_OK_BUTTON 0x1004
#define ID_CANCEL_BUTTON 0x1005

SearchWindow *SearchWindow::sw = nullptr;

SearchWindow::SearchWindow(
    HINSTANCE hInstance,
    const std::vector< std::pair< std::wstring, std::wstring > > &languages,
    const int lang_pos, const wchar_t *show, HWND parent_window )
        : parent( parent_window ), langs( languages ) {
    window = CreateWindowW( L"SearchWindow", _( GUI_WINDOW_SEARCH ),
                            WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME | WS_VISIBLE,
                            CW_USEDEFAULT, CW_USEDEFAULT, window_width,
                            window_height, parent, NULL, hInstance, NULL );
    ShowWindow( window, SW_SHOW );
    auto hFont = ( HFONT )GetStockObject( DEFAULT_GUI_FONT );
    EnableWindow( parent, false );
    setIcons( hInstance, window );

    std::wstring label = _( SHOW );
    label += L":";
    createLabel( label.c_str(), 5, 5, 30, 15, ID_SHOW_STATIC, window, hFont );

    label = _( ID_LANGUAGE );
    label += L":";
    createLabel( label.c_str(), 170, 5, 55, 15, ID_LANG_STATIC, window, hFont );

    label = _( POSSIBLE_SHOWS );
    label += L":";
    possible_label = createLabel( label.c_str(), 5, 60, 100, 15, ID_RES_STATIC,
                                  window, hFont );
    show_input = createEditBox( 5, 20, 160, 20, ID_SHOW_INPUT, window, hFont );
    SendMessage( show_input, WM_SETTEXT, NULL, ( LPARAM )show );
    lang_input = createCombo( 170, 20, 160, 250, ID_LANG_INPUT, window, hFont );
    for ( auto &x : languages ) {
        addItemToCombo( lang_input, x.second.c_str() );
    }
    SendMessage( lang_input, CB_SETCURSEL, lang_pos, NULL );

    createButton( _( SEARCH ), 340, 18, 80, 25, ID_SEARCH_BUTTON, window,
                  hFont );
    possible_input = createCombo( 5, 80, window_width - 125, 250, ID_RES_INPUT,
                                  window, hFont );
    createButton( _( OK ), window_width - 200, 110, 80, 25, ID_OK_BUTTON,
                  window, hFont );
    createButton( _( CANCEL ), window_width - 105, 110, 80, 25,
                  ID_CANCEL_BUTTON, window, hFont );

    ShowWindow( possible_label, SW_HIDE );
    ShowWindow( possible_input, SW_HIDE );

    UpdateWindow( window );
}

LRESULT CALLBACK SearchWindow::messageHandler( HWND hwnd, UINT umsg,
                                               WPARAM wParam, LPARAM lParam ) {
    switch ( umsg ) {
    case WM_CREATE:
        centerWindow( hwnd );
        break;
    case WM_COMMAND:
        switch ( LOWORD( wParam ) ) {
        case ID_OK_BUTTON:
            sw->ok();
            SendMessage( hwnd, WM_CLOSE, 0, 0 );
            break;
        case ID_CANCEL_BUTTON:
            sw->cancel();
            SendMessage( hwnd, WM_CLOSE, 0, 0 );
            break;
        case ID_SEARCH_BUTTON:
            sw->process();
            break;
        }
        break;
    case WM_DESTROY:
        PostQuitMessage( 0 );
        break;
    }
    return DefWindowProcW( hwnd, umsg, wParam, lParam );
}

void SearchWindow::mainLoop() {
    MSG msg;
    while ( GetMessage( &msg, NULL, 0, 0 ) ) {
        if ( !IsDialogMessage( window, &msg ) ) {
            TranslateMessage( &msg );
            sw = this;
            DispatchMessage( &msg );
        }
    }
}

void SearchWindow::process() {
    wchar_t show[256];
    SendMessage( show_input, WM_GETTEXT, ( WPARAM )255, ( LPARAM )show );
    if ( show[0] == '\0' ) {
        MessageBox( window, _( SHOW_FIELD_EMPTY ), _( ERROR ),
                    MB_OK | MB_ICONERROR );
        return;
    }

    auto index = SendMessage( lang_input, CB_GETCURSEL, NULL, NULL );
    auto lang_code = langs[index].first;

    possible_shows = searchShow( show, lang_code );
    if ( possible_shows.size() == 0 ) {
        MessageBox( window, _( NO_RESULTS ), _( ERROR ), MB_OK | MB_ICONERROR );
        return;
    }

    SendMessage( possible_input, CB_RESETCONTENT, NULL, NULL );
    for ( const auto &x : possible_shows ) {
        SendMessage( possible_input, CB_ADDSTRING, NULL,
                     ( LPARAM )x.first.c_str() );
    }
    SendMessage( possible_input, CB_SETCURSEL, 0, 0 );

    ShowWindow( possible_label, SW_SHOW );
    ShowWindow( possible_input, SW_SHOW );
}

void SearchWindow::ok() {
    selected = SendMessage( possible_input, CB_GETCURSEL, NULL, NULL );
    lang_id = SendMessage( lang_input, CB_GETCURSEL, NULL, NULL );
}

void SearchWindow::cancel() {
    selected = -1;
}

bool SearchWindow::accepted() {
    return selected != -1;
}

std::wstring SearchWindow::getShow() {
    return possible_shows[selected].first;
}

std::wstring SearchWindow::getShowID() {
    return possible_shows[selected].second;
}

int SearchWindow::getLangID() {
    return lang_id;
}
