#pragma comment( linker, "\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"" )
#include "seasonwindow.hpp"
#include "../functions.hpp"
#include "../resources_windows.h"
#include "gui_functions.hpp"

#define ID_CHECKBOX 0x0001
#define ID_SEASONS_STATIC 0x0001
#define ID_ALL_BUTTON 0x1000
#define ID_NONE_BUTTON 0x1001
#define ID_CONFIRM_BUTTON 0x1002

SeasonWindow *SeasonWindow::sw = nullptr;

SeasonWindow::SeasonWindow( HINSTANCE hInstance,
                            const std::vector< int > &seasons,
                            std::vector< int > &return_vector,
                            HWND parent_window )
        : parent( parent_window ), returned( return_vector ),
          season_nums( seasons ) {
    window = CreateWindowW( L"SeasonWindow", _( GUI_WINDOW_SEASON ),
                            WS_OVERLAPPEDWINDOW | WS_VISIBLE, CW_USEDEFAULT,
                            CW_USEDEFAULT, window_width, window_height, parent,
                            NULL, hInstance, NULL );
    ShowWindow( window, SW_SHOW );
    auto hFont = ( HFONT )GetStockObject( DEFAULT_GUI_FONT );
    EnableWindow( parent, false );
    setIcons( hInstance, window );

    size_t x{ 5 }, y{ 25 };

    // create a check box for each season
    HWND checkbox_hwnd;
    for ( auto &s : season_nums ) {
        checkbox_hwnd = createCheckbox( std::to_wstring( s ).c_str(), x, y, 55,
                                        15, ID_CHECKBOX, window, hFont );
        checkboxes.push_back( checkbox_hwnd );

        if ( x == 185 ) {
            x = 5;
            y += 20;
        } else {
            x += 60;
        }
    }

    std::wstring label = _( GUI_WINDOW_SEASON );
    label += L":";
    createLabel( label.c_str(), 5, 5, 100, 15, ID_SEASONS_STATIC, window,
                 hFont );
    createButton( _( SELECT_ALL ), 130, 175, 100, 25, ID_ALL_BUTTON, window,
                  hFont );
    createButton( _( SELECT_NONE ), 5, 175, 110, 25, ID_NONE_BUTTON, window,
                  hFont );
    createButton( _( SELECT ), 165, 215, 80, 25, ID_CONFIRM_BUTTON, window,
                  hFont );

    UpdateWindow( window );
}

void SeasonWindow::mainLoop() {
    MSG msg;
    while ( GetMessage( &msg, NULL, 0, 0 ) ) {
        if ( !IsDialogMessage( window, &msg ) ) {
            TranslateMessage( &msg );
            sw = this;
            DispatchMessage( &msg );
        }
    }
}

LRESULT CALLBACK SeasonWindow::messageHandler( HWND hwnd, UINT umsg,
                                               WPARAM wParam, LPARAM lParam ) {
    switch ( umsg ) {
    case WM_CREATE:
        centerWindow( hwnd );
        break;
    case WM_COMMAND:
        switch ( LOWORD( wParam ) ) {
        case ID_ALL_BUTTON:
            sw->selectAll();
            break;
        case ID_NONE_BUTTON:
            sw->selectNone();
            break;
        case ID_CONFIRM_BUTTON:
            sw->storeResult();
            SendMessage( hwnd, WM_CLOSE, 0, 0 );
            break;
        }
        break;
    case WM_DESTROY:
        PostQuitMessage( 0 );
        break;
    }
    return DefWindowProcW( hwnd, umsg, wParam, lParam );
}

void SeasonWindow::selectAll() {
    for ( auto &x : checkboxes ) {
        SendMessage( x, BM_SETCHECK, BST_CHECKED, NULL );
    }
}

void SeasonWindow::selectNone() {
    for ( auto &x : checkboxes ) {
        SendMessage( x, BM_SETCHECK, BST_UNCHECKED, NULL );
    }
}

void SeasonWindow::storeResult() {
    for ( unsigned long long i = 0; i < checkboxes.size(); i++ ) {
        if ( SendMessage( checkboxes[i], BM_GETCHECK, 0, 0 ) == BST_CHECKED ) {
            returned.push_back( season_nums[i] );
        }
    }
}
